execute pathogen#infect()

set hlsearch        "Highlight search

set expandtab       "Expand tabs to spaces
set softtabstop=2   "Soft tab stop width

set smartindent     "Smart indentation
set autoindent      "Automatic indentation.
set shiftwidth=2    "Number of spaces to automatically indent.

filetype plugin indent on "Sets indentation plugin
set omnifunc=syntaxcomplete#Complete

set number          "Show line numbers
"F2 toggles between setting numbers and setting foldcolumn for copy-pasting.
nnoremap <F2> :set nonumber!<CR>:set foldcolumn=0<CR>

set showcmd         "Displays an incomplete command before line,col.

set backspace=2

set spell           "Spell checking.


syntax on           "Syntax highlighting.

autocmd FileType python set complete+=k~/.vim/syntax/python.vim 

source ~/.vim/unicodemacros.vim


" Taglist variables
" Display function name in status bar:
let g:ctags_statusline=1

" Automatically start script
let generate_tags=1

" Displays tagslist results in a vertical window:
let Tlist_Use_Horiz_Window=0

" Shorter commands to toggle Taglist display
nnoremap TT :TlistToggle<CR>
map <F4> :TlistToggle<CR>

" Various Taglist display config:
let Tlist_Use_Right_Window = 1
let Tlist_Compact_Format = 1
let Tlist_Exit_OnlyWindow = 1
let TlisT_GainFocus_On_ToggleOpen = 1
let Tlist_File_Fold_Auto_Close = 1

set numberwidth=4
" Set columns to 84: numberwidth + 80 (normal line-number width)
set columns=84 

" autocmd VimEnter * NERDTree
" autocmd VimEnter * wincmd p
" autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" Close all open buffers on entering a window, if the
" only buffer that's left is the NERDTree buffer.
function! s:CloseIfOnlyNerdTreeLeft()
  if exists("t:NERDTreeBufName")
    if bufwinnr(t:NERDTreeBufName) != -1
      if winnr("$") == 1
        q
      endif
    endif
  endif
endfunction

colorscheme solarized
if has("gui_running")
  set lines=999
  set background=light
else
  set background=dark
endif


set wildmode=longest,list,full
set wildmenu

" Opam
let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"
execute "set rtp+=" . g:opamshare . "/merlin/vimbufsync"
